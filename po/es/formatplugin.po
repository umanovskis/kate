# Spanish translations for formatplugin.po package.
# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the kate package.
# Automatically generated, 2023.
#
# SPDX-FileCopyrightText: 2023 Eloy Cuadra <ecuadra@eloihr.net>
msgid ""
msgstr ""
"Project-Id-Version: formatplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-13 01:36+0000\n"
"PO-Revision-Date: 2023-11-27 00:52+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.3\n"

#: FormatApply.h:22 FormatApply.h:35 FormatPlugin.cpp:82 FormatPlugin.cpp:241
#: FormatterFactory.h:59
#, kde-format
msgid "Format"
msgstr "Formatear"

#: FormatApply.h:22
#, kde-format
msgid "Failed to write a temp file"
msgstr "No se ha podido escribir un archivo temporal"

#: FormatApply.h:35
#, kde-format
msgid "Failed to run git diff: %1"
msgstr "La ejecución de «git diff» ha fallado: %1"

#: FormatConfig.cpp:122
#, kde-format
msgid "User Settings"
msgstr "Preferencias del usuario"

#: FormatConfig.cpp:130
#, kde-format
msgid "Default Settings"
msgstr "Preferencias predeterminadas"

#: FormatConfig.h:19 FormatPlugin.cpp:112
#, kde-format
msgid "Formatting"
msgstr "Formato"

#: FormatConfig.h:24
#, kde-format
msgid "Formatting Settings"
msgstr "Preferencias del formato"

#: FormatPlugin.cpp:82
#, kde-format
msgid "Failed to read settings.json. Error: %1"
msgstr "No se ha podido leer «settings.json». Error: %1"

#: FormatPlugin.cpp:118
#, kde-format
msgid "Format Document"
msgstr "Formatear documento"

#: FormatPlugin.cpp:136
#, kde-format
msgid "Format on Save"
msgstr "Formatear al guardar"

#: FormatPlugin.cpp:139
#, kde-format
msgid "Disable formatting on save without persisting it in settings"
msgstr "Desactivar el formateo al guardar sin insistir en las preferencias"

#: FormatterFactory.h:59
#, kde-format
msgid "Failed to run formatter. Unsupported language %1"
msgstr "La ejecución del formateador ha fallado. Lenguaje %1 no reconocido."

#: Formatters.cpp:110
#, kde-format
msgid "%1: Unexpected empty command!"
msgstr "%1: Orden vacía no esperada."

#: Formatters.cpp:117
#, kde-format
msgid ""
"%1 is not installed, please install it to be able to format this document!"
msgstr "%1 no está instalado. Instálelo para poder formatear este documento."

#: Formatters.cpp:252
#, kde-format
msgid "Please install node and prettier"
msgstr "Instale «node» y «prettier»"

#: Formatters.cpp:259
#, kde-format
msgid "PrettierFormat: Failed to create temporary file"
msgstr "PrettierFormat: No se ha podido crear un archivo temporal"
